console.log('set advertisement');

var bleno = require('bleno');
var connected_device = process.env['connected_device'];
var regexp = new RegExp("..:..:..:..:..:..:..");
connected_device = regexp.exec(connected_device)[0].replace(/:/g, "");
console.log(connected_device);


var name = 'ohchang_test';
var serviceUuids = ['fffffffffffffffffffffffffffffff0']
var EIRDataString = '0502FFFFFFF0'
	+ '0816' + connected_device
	+ '06094348414E47'

bleno.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    //bleno.startAdvertising(name, serviceUuids, function(err) {
    bleno.startAdvertisingWithEIRData( new Buffer(EIRDataString, 'hex'), function(error){
      if (error !== null) {
        console.log(error); 
      }
      console.log('start advertisement');
      process.env['is_adv'] = true;
      setTimeout(function(){
        bleno.stopAdvertising();
        process.exit(1);
      }, 100000);
    });
  }
  else {
    bleno.stopAdvertising();
  }
});
/*
bleno.on('advertisingStart', function(error) {
  if (!error) {
    console.log('set service');
    bleno.setServices([
      new bleno.PrimaryService({
        uuid: 'fffffffffffffffffffffffffffffff0',
        characteristics : [
          new bleno.Characteristic({ 
            uuid: 'fffffffffffffffffffffffffffffff1',
            properties: ['notify'],
            value: new Buffer(connected_device)
          })
        ]
      })
    ]);
  } 
});
*/
