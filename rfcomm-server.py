from bluetooth import *
import os

serverSock=BluetoothSocket(RFCOMM)
serverSock.bind(("", 1))
serverSock.listen(1)

host = serverSock.getsockname()[0]
port = serverSock.getsockname()[1]

print "Waiting for connection on RFCOMM host %s channel %d" % (host, port)

clientSock, clientInfo = serverSock.accept();

print "Accepted connection from ", clientInfo

file_path="/tmp/pid_bluetooth0"
try:
    while True:
        pid_file=open(file_path, 'w+')
        data = clientSock.recv(1024)
        if len(data) == 0: break
        print >> pid_file, "%s" % data
except IOError:
    pass

print "disconnected"
pid_file.close();
os.remove(file_path)

clientSock.close()
serverSock.close()
print "all done"
